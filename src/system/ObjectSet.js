/**
 * Created by Gilles on 25.03.2017.
 */
class ObjectSet extends Set {
  constructor(iteratable) {
    super(iteratable);
  }
  
  has(value) {
    if(typeof value != "object") {
      return super.has(value);
    }
    
    var values = this.values();
    if(!values) return false;
    for(let val of values) {
      var equals = false;
      if(typeof val == "object") {
        if(val.equals) {
          equals= val.equals(value);
        } else {
          equals = JSON.stringify(val) == JSON.stringify(value);
        }
      }
      if(equals) {
        return true;
      }
    }
    return false;
  }
}

module.exports = ObjectSet;
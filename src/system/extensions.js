/**
 * Created by Gilles on 25.03.2017.
 */
module.exports = function() {
  
  String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  };
  
  
  global.source = function(src) {
    return require(cwd+"/src/"+src);
  };
  
  global.ObjectSet = source("system/ObjectSet");
  
  
};



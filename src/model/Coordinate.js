/**
 * Created by Gilles on 25.03.2017.
 */
class Coordinate {
  constructor(x,y) {
   this.x = parseInt(x);
   this.y = parseInt(y);
  }
  
  equals(coordinate) {
    let coord = Coordinate.fromString(coordinate);
    if(coordinate != Coordinate.ILLEGAL_COORDINATE) {
      return this.x === coordinate.x && this.y === coordinate.y;
    }
    return false;
  }
  
  inBound(maxX, maxY) {
    if(this.x >= 1 && this.x <= maxX) {
      if(this.y >= 1 && this.y <= maxY) {
        return true;
      }
    }
    return false;
  }
  
  static fromString(xAndY) {
    if(xAndY instanceof Coordinate) {
      return xAndY;
    }
  
    if(typeof xAndY == "string") {
      let splitted = xAndY.split(",");
      if(splitted.length === 2) {
        return new Coordinate(splitted[0], splitted[1]);
      }
    }
    return Coordinate.ILLEGAL_COORDINATE;
  }
}

Coordinate.ILLEGAL_COORDINATE = Symbol("ILLEGAL_COORDINATE");


module.exports = Coordinate;
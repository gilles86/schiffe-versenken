/**
 * Created by Gilles on 25.03.2017.
 */
let Coordinate = source("model/Coordinate");
let Shot = source("model/Shot");
let Coordinates = source("model/Coordinates");



class PlayerBoard {
  /**
   *
   * @param {Number} width
   * @param {Number} height
   * @param {Array.<Ship>} ships
   */
  constructor(width, height, ships) {
    this.width = width;
    this.height = height;
    this.ships = ships||[];
    this.shots = new ObjectSet();
    this.shotsObj = [];
  }
  
  shuffleShips() {
    this.canPlaceAllShips(); // Throws error if a ships cannot be placed
    
    let ships = [];
    for(let ship of this.ships) {
      ship.shufflePosition(this.width, this.height);
      while(ship.hasCollision(ships)) {
        ship.shufflePosition(this.width, this.height);
      }
      ships.push(ship);
    }
  }
  
  isShipOnCoordinate(coord) {
    for(let ship of this.ships) {
      if(ship.isOnCoordinate(coord)) {
        return true;
      }
    }
    return false;
  }
  
  canPlaceAllShips() {
    let fieldCount = this.width * this.height;
    let fieldsTaken = this.getShipSpaceNeeded();
    let fits = (fieldCount > fieldsTaken);
    if(!fits) {
      throw new Error("Please edit the GameConfig: You have "+fieldCount+" possible fields and the ship take space up for "+fieldsTaken)
    }
    return fits;
  }
  
  getShipSpaceNeeded() {
    let fieldsTaken = 0;
    for(let ship of this.ships) {
      fieldsTaken += ship.shipLength;
    }
    return fieldsTaken;
  }
  
  
  shoot(x,y) {
    let shot = new Shot(x,y);
    if(shot.inBound(this.width, this.height)) {
      shot.hit = this.isShipOnCoordinate(shot);
      if(!this.shots.has(shot)) {
        this.shots.add(shot);
        this.shotsObj = Array.from(this.shots);
        return shot.hit;
      }
    }
    return false;
  }
  
  getHits() {
    return [...this.shots].filter(function(shot) {
      return shot.hit;
    })
  }
  
  
  /**
   *
   * @param {Ship} ship
   * @param {Number} x
   * @param {Number} y
   * @param {DIRECTION} direction
   */
  setShip(ship, x, y, direction) {
    let coords = Coordinates.fromStartPointWithLength(x,y,ship.shipLength,direction);
    
  }
  
  
  
}

module.exports = PlayerBoard;
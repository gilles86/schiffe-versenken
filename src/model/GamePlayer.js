/**
 * Created by Gilles on 25.03.2017.
 */
let PlayerBoard = source("model/PlayerBoard");
let Ship = source("model/Ship");

class GamePlayer {
  constructor(name, gameConfig) {
    if(name) {
      this.name = name;
      this.board = new PlayerBoard(gameConfig.fieldSize,gameConfig.fieldSize, this.createShipsForBoard(gameConfig.ships));
      this.board.shuffleShips();
    }
  }
  
  createShipsForBoard(shipsArray) {
    let ships = [];
    for(let shipString of shipsArray) {
      ships.push(new Ship(parseInt(shipString.trim())));
    }
    return ships;
  }
  
  shoot(x,y) {
    return this.board && this.board.shoot(x,y);
  }
  
}


module.exports = GamePlayer;
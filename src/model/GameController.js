/**
 * Created by Gilles on 25.03.2017.
 */
let extend = require("extend");
let Game = source("model/Game");


class GameController {
  
  constructor() {
    this.game = null;
  }
  
  createGame(name) {
    this.game = new Game();
    this.game.createPlayer(1, name);
  }
  
  joinGame(name) {
    if(this.game && this.game.player1) {
      this.game.createPlayer(2, name);
    }
  }
  
  shoot(number, x, y) {
    return this.game.shoot(number,x,y);
  }
  
  actionCheck(playerNumber, params, res) {
    res.send(this.createJsonModel({
      "ready" : this.isGameReady()
    }));
  }
  
  actionShoot(playerNumber, params, res) {
    let hit = !!this.game.shoot(playerNumber, params["x"], params["y"]);
    res.send(this.createJsonModel({
      "status" : this.game.hasPlayerWon(),
      "shotHit" : hit
    }));
  }
  
  actionShuffle(playNumber, params, res) {
    if(this.game.player1) {
      this.game.player1.board.shuffleShips();
    }
  
    if(this.game.player2) {
      this.game.player2.board.shuffleShips();
    }
    res.send("{'status' : 'ok'}");
  }
  
  
  isGameReady() {
    return !!(this.game && this.game.player1 && this.game.player2);
  }
  
  /**
   *
   * @param view
   * @param [model]
   * @returns {{view: *, model}}
   */
  createViewModel(view, model) {
    return {
      "view" : view,
      "model" : this.createModel(model)
    }
  }
  
  createModel(model={}) {
    return extend({}, {"game" : this.game}, model);
  }
  
  createJsonModel(model) {
    return JSON.stringify(this.createModel(model));
  }
  
  
  
}

module.exports = GameController;
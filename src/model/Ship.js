/**
 * Created by Gilles on 25.03.2017.
 */
let DIRECTION = source("model/DIRECTION");
let randomInt = require('random-int');
let Coordinate = source("model/Coordinate");
let Coordinates = source("model/Coordinates.js");

class Ship {
  constructor(shipLength) {
    this.shipLength = parseInt(shipLength);
    this.x = null;
    this.y = null;
    
    /** @type {DIRECTION} */
    this.direction = null;
    
    /** @type {Coordinates} */
    this.coordinates = null;
  }
  
  /**
   *
   * @param boundX
   * @param boundY
   * @return {Coordinates}
   */
  shufflePosition(boundX, boundY) {
    if(boundX < this.shipLength || boundY < this.shipLength) {
      console.error("Bounds for ships must be greater than the ship size", this.shipLength, boundX, boundY);
      throw new Error("Bounds for ships must be greater than the ship size");
    }
    
    this.x = randomInt(1, boundX);
    this.y = randomInt(1, boundY);
    this.direction = (randomInt(1) == 1) ? DIRECTION.HORIZONTAL : DIRECTION.VERTICAL;
    this.directionString = (this.direction == DIRECTION.VERTICAL) ? "vertical" : "horizontal";
    if(!this.fitsIn(boundX, boundY)) {
      return this.shufflePosition(boundX, boundY);
    }
    this.coordinates = Coordinates.fromStartPointWithLength(this.x, this.y, this.shipLength, this.direction);
    return this.coordinates;
  }
  
  
  fitsIn(boundX, boundY) {
    let coords = Coordinates.fromStartPointWithLength(this.x, this.y, this.shipLength, this.direction);
    return !coords.exceedsBounds(boundX,boundY);
  }
  
  isOnCoordinate(coord) {
    return this.coordinates.hasCoord(coord);
  }
  
  
  /**
   * @param {Array.<Ship>} ships
   */
  hasCollision(ships) {
    for(let checkShip of ships) {
      if(checkShip.coordinates.hasAnyCoord(this.coordinates)) {
        return true;
      }
    }
    return false;
  }
  
}

module.exports = Ship;

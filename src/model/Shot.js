const Coordinate = source("model/Coordinate");

/**
 * Ein Schuss auf einer Karte
 */
class Shot extends Coordinate {
  constructor(x,y) {
    super(x,y);
    this.hit = false;
  }
}

module.exports = Shot;
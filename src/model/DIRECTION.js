/**
 * Created by Gilles on 26.03.2017.
 */
/**
 * @type {{HORIZONTAL: Symbol, VERTICAL: Symbol}}
 * @enum {Symbol}
 */
let DIRECTION = {
  "HORIZONTAL" : Symbol("direction_horizontal"),
  "VERTICAL" : Symbol("direction_vertical")
};

module.exports = DIRECTION;
/**
 * Created by Gilles on 25.03.2017.
 */
let GAMECONFIG = require(cwd+"/config.js");
let GamePlayer = source("model/GamePlayer");

class Game {
  constructor() {
    this.player1 = null;
    this.player2 = null;
    this.status = null;
    this.config = GAMECONFIG;
  }
  
  createPlayer(num,name) {
    if(num === 1 || num === 2) {
      this["player"+num] = new GamePlayer(name, this.config);
    }
  }
  
  shoot(playernum, x,y) {
    let player = this.getPlayer((playernum == "1") ? 2 : 1);
    return player.shoot(x,y);
  }
  
  hasPlayerWon() {
    
    let neededHits = this.player1.board.getShipSpaceNeeded();
    
    let player1Won = this.player1.board.getHits() >= neededHits;
    let player2Won = this.player2.board.getHits() >= neededHits;
    if(player1Won) {
      return 1;
    }
    
    if(player2Won) {
      return 2;
    }
    
    return false;
  }
  
  /**
   *
   * @param {number} num
   * @return {GamePlayer}
   */
  getPlayer(num) {
    num = parseInt(num);
    if(num === 1 || num === 2) {
      return this["player"+num];
    }
    console.warn("get Player only accepts 1 or 2 as parameter");
    return new GamePlayer(null);
  }
  
  
  
}

module.exports = Game;
/**
 * Created by Gilles on 25.03.2017.
 */
let Coordinate = source("model/Coordinate");
let DIRECTION = source("model/DIRECTION");

class Coordinates {
  constructor(...coords) {
    this.coordinates = new ObjectSet(coords);
    this.coordinatesArray = Array.from(this.coordinates);
  }
  
  hasCoord(coord) {
    return this.coordinates.has(coord);
  }
  
  add(coord) {
    if(coord instanceof Coordinate) {
      this.coordinates.add(coord);
    }
  }
  
  exceedsBounds(boundX, boundY) {
    return this.exceedsBoundType(boundX,true)||this.exceedsBoundType(boundY, false);
  }
  
  /**
   *
   * @param bound
   * @param {boolean} isX
   * @return {boolean}
   */
  exceedsBoundType(bound, isX) {
    let boundAttr = (isX) ? "x" : "y";
    
    for(let coord of this.coordinates) {
      if(coord[boundAttr] > bound) {
        return true;
      }
    }
    return false;
  }
  
  /**
   *
   * @param {Iterable.<Coordinate>} coords
   */
  hasAnyCoord(coords) {
    for(let coord of coords) {
      if(this.hasCoord(coord)) {
        return true;
      }
    }
    return false;
  }
  
  
  
  static fromStrings(...stringCoords) {
    let coords = new ObjectSet(); // Set damit doppelte Koordinaten rausgefiltert werden
    for(let coordString of stringCoords) {
      let coord = Coordinate.fromString(coordString);
      coords.add(coord);
    }
    return new Coordinates(...coords);
  }
  
  /**
   *
   * @param {Number} x
   * @param {Number} y
   * @param {Number} length
   * @param {DIRECTION} direction
   *
   * @return {Coordinates}
   */
  static fromStartPointWithLength(x, y, length, direction) {
    let coords = [new Coordinate(x,y)];
    for(let i=1; i < length; i++) {
      if(direction == DIRECTION.VERTICAL) {
        y++;
      } else {
        x++;
      }
      coords.push(new Coordinate(x,y));
    }
    return Coordinates.fromStrings(...coords);
  }
  
  *[Symbol.iterator]() {
    for(let coord of this.coordinates) {
      yield coord;
    }
  }
}



module.exports = Coordinates;
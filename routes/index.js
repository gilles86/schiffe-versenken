let express = require('express');
let router = express.Router();
let GameController = require(cwd+"/src/model/GameController.js");

let gameController = new GameController();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render("index", {"game" : gameController.game});
});

router.get("/start/",function(req, res, next) {
  let name = req.query.name;
  if(name) {
    gameController.createGame(name);
    res.render("game", {"game" : gameController.game, "playernumber" : 1});
  } else {
    res.render("error");
  }
  
});

router.get("/join/", function(req, res, next) {
  let name = req.query.name;
  if(name && gameController.game) {
    gameController.joinGame(name);
    res.render("game", {"game" : gameController.game, "playernumber" : 2});
  } else {
    res.render("error");
  }
});

router.get("/game/:playernumber/:action", function(req, res, next) {
  let playerNumber = req.params.playernumber;
  let action = req.params.action;
  let params = req.query;
  
  var functionexists = !!gameController["action"+(action.capitalizeFirstLetter())];
  
  if(functionexists && gameController.game) {
    let viewAndModel = gameController["action"+(action.capitalizeFirstLetter())](playerNumber, params, res);
    if(viewAndModel) {
      res.render(viewAndModel.view, viewAndModel.model);
    }
  } else {
    res.render("error");
  }
  
});

module.exports = router;

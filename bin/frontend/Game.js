class Game {
  constructor(player) {
    this.playerNo     = player;
    this.ajaxUrl      = "/game/" + this.playerNo + "/check";
    this.ajaxInterval = 2;

    this.lastData  = {};
    this.boardDrawn = false;

  }

  gameLoop() {
    setTimeout(() => {
      $.ajax({
        url:      this.ajaxUrl,
        dataType: "json"
      }).done((data) => {
        this.gameLoop();
        this.processData(data);
      });
    }, this.ajaxInterval * 1000);
  }

  processData(data) {
    this.lastData = data;
    console.log(data);

    if (!this.boardDrawn) {
      this.drawBoard();
      this.drawShips();
    } else {
      this.drawShips();
    }
  }

  drawBoard() {
    let boardPlayer1 = {};
    let boardPlayer2 = {};

    if (this.lastData.game.player1 !== null && this.lastData.game.player2 !== null) {
      boardPlayer1.width  = this.lastData.game.player1.board.width;
      boardPlayer1.height = this.lastData.game.player1.board.height;

      boardPlayer2.width  = this.lastData.game.player2.board.width;
      boardPlayer2.height = this.lastData.game.player2.board.height;


      let $tableP1Html = $("<table></table>");
      let $tableHeadP1 = $("<thead><tr><td colspan='"+boardPlayer1.width+"'>"+this.lastData.game.player1.name+"</td></tr></thead>");
      $tableP1Html.append($tableHeadP1);

      for (let h = 1; h <= boardPlayer1.height; h++) {
        let $newRow = $("<tr></tr>");
        for (let w = 1; w <= boardPlayer1.width; w++) {
          $newRow.append("<td class='" + h + "-" + w + "'>" + h + "-" + w + "</td>");
        }
        $tableP1Html.append($newRow);
      }

      $("#board-p1").html($tableP1Html);



      let $tableP2Html = $("<table></table>");
      let $tableHeadP2 = $("<thead><tr><td colspan='"+boardPlayer2.width+"'>"+this.lastData.game.player2.name+"</td></tr></thead>");
      $tableP2Html.append($tableHeadP2);

      for (let h = 1; h <= boardPlayer2.height; h++) {
        let $newRow = $("<tr></tr>");
        for (let w = 1; w <= boardPlayer2.width; w++) {
          $newRow.append("<td class='" + h + "-" + w + "'>" + h + "-" + w + "</td>");
        }
        $tableP2Html.append($newRow);
      }

      $("#board-p2").html($tableP2Html);

      this.boardDrawn = true;
    }
  }

  drawShips() {
    let myData = {};
    if(this.playerNo === 1) {
      myData = this.lastData.game.player1.board.ships;
    } else {
      myData = this.lastData.game.player2.board.ships;
    }

    for (let ship of myData) {
      for(let coordinate of ship.coordinates.coordinatesArray) {
        let coordinateClass = "."+coordinate.x + "-" + coordinate.y;
        $(".currentPlayerBoard "+coordinateClass).addClass("ship");
      }
    }
  }
}
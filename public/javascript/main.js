class Game {
  constructor(player) {
    this.playerNo     = player;
    this.ajaxUrl      = "/game/" + this.playerNo + "/check";
    this.shotUrl      = "/game/" + this.playerNo + "/shoot";
    this.ajaxInterval = 2;

    this.lastData  = {};
    this.boardDrawn = false;
    
    this.yCoordMapping = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
    this.yCoordMapping.unshift(null);

  }
  
  addEvents() {
   $(".enemyPlayerBoard [data-coords]").on("click", (evt) => {
     let $clickedElem = $(evt.target);
     let coords = $clickedElem.attr("data-coords").split("-");
     this.sendShot(coords[1], coords[0]);
   });
  }
  
  sendShot(x,y) {
    $.ajax({
      url:      this.shotUrl,
      dataType: "json",
      "data" : {
        "x" : x,
        "y" : y
      }
    }).done((data) => {
      this.gameLoop();
      this.processData(data);
    });
  }

  gameLoop() {
    setTimeout(() => {
      $.ajax({
        url:      this.ajaxUrl,
        dataType: "json"
      }).done((data) => {
        this.gameLoop();
        this.processData(data);
      });
    }, this.ajaxInterval * 1900);
  }

  processData(data) {
    this.lastData = data;
    console.log(data);

    if (!this.boardDrawn) {
      this.drawBoard();
      this.drawShips();
      this.addEvents();
    } else {
      this.drawShips();
      this.drawShoots(1);
      this.drawShoots(2);
    }
  }

  drawBoard() {
    let boardPlayer1 = {};
    let boardPlayer2 = {};

    if (this.lastData.game.player1 !== null && this.lastData.game.player2 !== null) {
      boardPlayer1.width  = this.lastData.game.player1.board.width;
      boardPlayer1.height = this.lastData.game.player1.board.height;

      boardPlayer2.width  = this.lastData.game.player2.board.width;
      boardPlayer2.height = this.lastData.game.player2.board.height;


      let $tableP1Html = $("<table></table>");
      let $tableHeadP1 = $("<thead><tr><td colspan='"+boardPlayer1.width+"'>"+this.lastData.game.player1.name+"</td></tr></thead>");
      $tableP1Html.append($tableHeadP1);

      for (let h = 1; h <= boardPlayer1.height; h++) {
        let $newRow = $("<tr></tr>");
        for (let w = 1; w <= boardPlayer1.width; w++) {
          let datacoords =  "data-coords='"+ h + "-" + w +"'" ;
          $newRow.append("<td class='" + h + "-" + w + "' "+datacoords+">" + this.yCoordMapping[h] + "-" + w + "</td>");
        }
        $tableP1Html.append($newRow);
      }

      $("#board-p1").html($tableP1Html);



      let $tableP2Html = $("<table></table>");
      let $tableHeadP2 = $("<thead><tr><td colspan='"+boardPlayer2.width+"'>"+this.lastData.game.player2.name+"</td></tr></thead>");
      $tableP2Html.append($tableHeadP2);

      for (let h = 1; h <= boardPlayer2.height; h++) {
        let $newRow = $("<tr></tr>");
        for (let w = 1; w <= boardPlayer2.width; w++) {
          let datacoords =  "data-coords='"+ h + "-" + w +"'";
          $newRow.append("<td class='" + h + "-" + w + "' "+datacoords+">" + this.yCoordMapping[h] + "-" + w + "</td>");
        }
        $tableP2Html.append($newRow);
      }

      $("#board-p2").html($tableP2Html);

      this.boardDrawn = true;
    }
  }

  drawShips() {
    let myData = {};
    if(this.playerNo === 1) {
      myData = this.lastData.game.player1.board.ships;
    } else {
      myData = this.lastData.game.player2.board.ships;
    }
    $(".currentPlayerBoard .ship").removeClass("ship");
    for (let ship of myData) {
      for(let coordinate of ship.coordinates.coordinatesArray) {
        let coordinateClass = "."+coordinate.y + "-" + coordinate.x;
        $(".currentPlayerBoard "+coordinateClass).addClass("ship");
      }
    }
  }
  
  drawShoots(num) {
    let myData = {};
    if(num === 1) {
      myData = this.lastData.game.player1.board.shotsObj;
    } else {
      myData = this.lastData.game.player2.board.shotsObj;
    }
    for(let shot of myData) {
      let coordClass = "."+shot.y + "-" + shot.x
      let $field = $("#board-p"+num+" "+coordClass);
      $field.addClass(shot.hit ? "hit" : "shot");
    }
  }
}
$(document).ready(function() {
  var game = new Game(window.playerNumber);
  game.gameLoop();
});
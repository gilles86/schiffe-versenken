var gulp = require("gulp");

var concat = require("gulp-concat");

gulp.task("frontend", function() {
  return gulp.src("bin/frontend/*.js")
    .pipe(concat("main.js"))
    .pipe(gulp.dest("public/javascript/"));
});

gulp.task("default", ['frontend']);
